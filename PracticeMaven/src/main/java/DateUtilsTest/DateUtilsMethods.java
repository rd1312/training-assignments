package DateUtilsTest;

import org.apache.commons.lang3.time.DateUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtilsMethods {
    public static void main(String[] args) {
        Date currDate = new Date();
        int daysToAdd = 5;
        int newMonthValue=3;
        int newYearValue = 2030;
        int minutesToAdd = 30;

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss:SS");

        Date newDate = DateUtils.setYears(currDate, newYearValue);
        System.out.printf("DateUtils.setYears(%s, %s) = %s\n", dateFormat.format(currDate), newYearValue, dateFormat.format(newDate));

        Date setMonths = DateUtils.setMonths(currDate, newMonthValue);
        System.out.printf("DateUtils.setMonths(%s, %s) = %s\n", currDate, newMonthValue, setMonths);

        Date addDate = DateUtils.addDays(currDate,daysToAdd);
        System.out.printf("%s + %s days = %s\n",currDate, daysToAdd, addDate);

        Date addMinutes = DateUtils.addMinutes(currDate, minutesToAdd);
        System.out.printf("%s + %s minutes = %s\n", currDate, minutesToAdd, addMinutes);


    }
}
