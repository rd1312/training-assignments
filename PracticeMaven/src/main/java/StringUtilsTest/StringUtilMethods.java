package StringUtilsTest;

import org.apache.commons.lang3.StringUtils;

public class StringUtilMethods {
    public static void main(String[] args) {
        String string ="Java is a genera-purpose object oriented programming language";
        System.out.println("Using StringUtils.indexOf():-");
        System.out.println(StringUtils.indexOf(null, "a"));
        System.out.println(StringUtils.indexOf(string, "general"));
        System.out.println(StringUtils.indexOf(string, "l"));
        System.out.println(StringUtils.indexOf(string, "lan"));

        System.out.println("Using StringUtils.lastIndexOf():-");
        System.out.println(StringUtils.lastIndexOf(null, "a"));
        System.out.println(StringUtils.lastIndexOf(string, "object"));
        System.out.println(StringUtils.lastIndexOf(string, "l"));
        System.out.println(StringUtils.lastIndexOf(string, "lan"));

        String string1 = "java is a programming language";
        System.out.println(StringUtils.abbreviate(string1,24));

        System.out.println(StringUtils.capitalize(string1));

        System.out.println(StringUtils.countMatches(string1,"a"));

        String string2 = "java version 11";
        System.out.println(StringUtils.getDigits(string2));

    }
}
