package StringUtilsTest;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;

public class StringUtilDemo {
    public static void main(String[] args) {
//        System.out.println(StringUtils.isBlank(""));
//        System.out.println(StringUtils.isBlank("rahul"));
//        System.out.println(StringUtils.isEmpty("rahul"));
//        System.out.println(StringUtils.isEmpty(""));
//        System.out.println(StringUtils.isAllUpperCase("RAHUL"));

        String nullString = null;
        String emptyString = "";
        String blankString = "\n \t   \n";
//  isEmpty() and isBlank()
//        if(!nullString.isEmpty()) {
//            System.out.println("nullString isn't null");
//        }
        if(StringUtils.isEmpty(nullString)) {
            System.out.println("emptyString is empty");
        }
        if(StringUtils.isBlank(nullString)) {
            System.out.println("blankString is blank");
        }
        if(StringUtils.isEmpty(emptyString)) {
            System.out.println("emptyString is empty");
        }
        if(StringUtils.isBlank(blankString)) {
            System.out.println("blankString is blank");
        }

        //StringUtils.equals()
        List<String> names = Arrays.asList("Arnie", "Lucy", "Beth", "Amir");

        System.out.println(names);
        String joinedWithComma = StringUtils.join(names, ",");
        String joinedNoSeparator = StringUtils.join(names, null);
        System.out.println(joinedWithComma);
        System.out.println(joinedNoSeparator);

        String sentence = "The cat leapt over the big brown dog.";

        // The cat leapt...
        String abbreviated = StringUtils.abbreviate(sentence, 16);
        System.out.println(abbreviated);
    }
}
