package StringUtilsTest;

import org.apache.commons.lang3.StringUtils;

public class EqualsAndCompare {
    public static void main(String[] args) {

        //equals() method
        String Str1 = "some string";
        String Str2 = "some other string";
        System.out.println(StringUtils.equals(null, null));
        System.out.println(StringUtils.equals(null, Str1));
        System.out.println(StringUtils.equals(Str1, null));
        System.out.println(StringUtils.equals(Str1, Str1));
        System.out.println(StringUtils.equals(Str2, Str1));

        //compare() method

        System.out.println(StringUtils.compare(null, null));
        System.out.println(StringUtils.compare(null, Str1));
        System.out.println(StringUtils.compare(Str1, null));
        System.out.println(StringUtils.compare(Str1, Str1));
        System.out.println(StringUtils.compare(Str1, Str2));

    }
}
